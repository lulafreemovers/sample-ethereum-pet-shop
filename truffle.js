const PrivateKeyProvider = require("truffle-privatekey-provider");

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  networks: {
    development: {
      //host: "http://bops.morpheuslabs.io",
      //port: 20801,
      provider: () => new PrivateKeyProvider("7604e362eaeff0af5564ac2d5ff46201f17a2ebffb4ed88b6d8511e73e3bdf94", "http://bops.morpheuslabs.io:20801"),
      network_id: "*" // Match any network id
    }
  }
};
